package com.balaji.simple_lib

object SimpleClass {

    fun addition(count1: Int, count2: Int): Int {
        return count1 + count2
    }
}