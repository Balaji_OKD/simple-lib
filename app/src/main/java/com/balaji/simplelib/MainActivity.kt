package com.balaji.simplelib

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.balaji.simple_lib.SimpleClass
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvTextView.text = SimpleClass.addition(2, 5).toString()
    }
}